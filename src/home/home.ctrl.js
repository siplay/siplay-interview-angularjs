import helper from '../controller.helper';

import './home.scss';

export default class HomeCtrl {

    constructor() {
    }

}

helper.registerRoute('/', HomeCtrl, require('./home.pug'));