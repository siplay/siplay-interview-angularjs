import helper from './controller.helper';

import './instructions.scss';

export default class InstructionsCtrl {

    constructor() {
    }

}

helper.registerDirective('instructions', InstructionsCtrl, require('../README.md'));