import helper from '../controller.helper';

import './tasks.scss';

export default class TasksCtrl {

    /** TODO Your Tasks app goes here **/

    constructor() {
        this.instantiated = true;
    }

}

helper.registerRoute('/tasks', TasksCtrl, require('./tasks.pug'));