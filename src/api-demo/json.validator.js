import controllerHelper from '../controller.helper';

export default class JsonValidatorCtrl {

    constructor($element) {
        let ngModel = $element.controller('ngModel');
        ngModel.$parsers.unshift(value => {
            try {
                let result = JSON.parse(value);
                ngModel.$setValidity('json', true);
                return result;
            }
            catch(ex) {
                ngModel.$setValidity('json', false);
                return undefined;
            }
        });
    }

}

controllerHelper.registerDirective('json', JsonValidatorCtrl, null, {
    require: 'ngModel'
});