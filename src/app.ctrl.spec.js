import AppCtrl from './app.ctrl';

describe('AppCtrl', () => {

    let module;

    beforeEach(() => {
        module = angular.module('app', ['ngRoute']);
        angular.mock.module('app');
        AppCtrl.registerDirective(module);
    });

    describe('.ctr', () => {

        let ctrl, $scope;

        beforeEach(() => {
            angular.mock.inject(($controller, $rootScope) => {
                $scope = $rootScope.$new();
                ctrl = $controller('AppCtrl', { $scope });
            });
        });

        it('puts $location on the scope', () => {
            expect($scope.$location).toBeDefined();
            expect($scope.$location.path).toBeDefined();
            expect($scope.$location.path()).toBe('');
        });

    });

});