import controllerHelper from './controller.helper';

describe('controllerHelper', () => {

    let module, TestCtrl;


    beforeEach(() => {
        module = angular.module('app', ['ngRoute']);
        angular.mock.module('app');

        TestCtrl = function TestCtrl() {
            this.message = 'test';
        };
    });

    describe('registerDirective', () => {

        beforeEach(() => {
            controllerHelper.registerDirective('test', TestCtrl, 'test controller!');
        });

        it('adds a static `registerDirective` function to the controller', () => {
            expect(TestCtrl.registerDirective).toBeDefined();
        });

        it('registers a controller for the directive', () => {
            TestCtrl.registerDirective(module);
            angular.mock.inject($controller => {
                let testCtrl = $controller('TestCtrl');
                expect(testCtrl).toBeDefined();
                expect(testCtrl.message).toEqual('test');
            });
        });

        it('registers a directive', () => {
            TestCtrl.registerDirective(module);
            angular.mock.inject(($compile, $rootScope) => {
                let el = $compile('<test></test>')($rootScope);
                $rootScope.$digest();
                expect(el.html()).toEqual('test controller!');
            });
        });

    });

    describe('registerRoute', () => {

        beforeEach(() => {
            controllerHelper.registerRoute('/test', TestCtrl, 'test route!');
        });

        it('adds a static `registerRoute` function to the controller', () => {
            expect(TestCtrl.registerRoute).toBeDefined();
        });

        it('registers a controller for the route', () => {
            TestCtrl.registerRoute(module);
            angular.mock.inject($controller => {
                let testCtrl = $controller('TestCtrl');
                expect(testCtrl).toBeDefined();
                expect(testCtrl.message).toEqual('test');
            });
        });

    });

});