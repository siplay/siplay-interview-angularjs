import helper from './controller.helper';

import './app.scss';

export default class AppCtrl {

    constructor($scope, $location) {

        $scope.$location = $location;
    }

}

helper.registerDirective('app', AppCtrl, require('./app.pug'));