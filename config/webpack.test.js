module.exports = {

    devtool: 'inline-source-map',

    module: {
        rules: [
            {
                enforce: 'post',
                test: /\.js$/,
                use: {
                    loader: 'istanbul-instrumenter-loader',
                    options: {
                        esModules: true
                    }
                },
                exclude: [
                    /node_modules/,
                    /\.spec\.js$/
                ]
            }
        ]
    }

};